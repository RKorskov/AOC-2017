// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-03-21 17:09:22 korskov>

/*
  http://adventofcode.com/2017
*/

import java.lang.reflect.Constructor;
import java.lang.Class;

//import java.io.Console; // System.console ();
// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
// import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Scanner; // Scanner (System.in);
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;


public class aoc2017_00 {

    public static boolean __DEBUG__ = false;

    public static void main (String[] args) throws Exception {
        new aoc2017_00().worker(args);
    }


    private void worker (final String[] args) {
        String nn = "aoc2017_" + args[0];
        // System.out.println (nn);
        try {
            Class <?> aoc_cls = Class.forName (nn);
            Constructor <?> aoc_cns = aoc_cls.getConstructor();
            AoC2017 aoc_o = (AoC2017) aoc_cns.newInstance();
            aoc_o.eval (args);
        }

        catch (Exception ex) {
            System.out.println ("... oops! with " + nn);
        }
        return;
    }
}

