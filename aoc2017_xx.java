// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-03-21 18:03:02 korskov>

/*
  http://adventofcode.com/2017/day/X
*/

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
// import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Scanner; // Scanner (System.in);
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;


public class aoc2017_xx implements AoC2017 {

    public void eval (final String[] args) {
        System.out.println (args[0]);
        System.out.println (first (args[1]));
        System.out.println (second (args[2]));
    }

    
    private String first (final String str) {
        return "xx first";
    }


    private String second (final String str) {
        return "xx second";
    }

}
