// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-04-05 16:53:36 korskov>

/*
  http://adventofcode.com/2017/day/3

  --- Day 3: Spiral Memory ---

  You come across an experimental new kind of memory stored on an
  infinite two-dimensional grid.

  Each square on the grid is allocated in a spiral pattern starting at
  a location marked 1 and then counting up while spiraling
  outward. For example, the first few squares are allocated like this:

  17  16  15  14  13  ...
  18   5   4   3  12  29
  19   6   1   2  11  28
  20   7   8   9  10  27
  21  22  23  24  25  26

  While this is very space-efficient (no squares are skipped),
  requested data must be carried back to square 1 (the location of the
  only access port for this memory system) by programs that can only
  move up, down, left, or right. They always take the shortest path:
  the Manhattan Distance between the location of the data and square
  1.

  For example:

    Data from square 1 is carried 0 steps, since it's at the access port.
    Data from square 12 is carried 3 steps, such as: down, left, left.
    Data from square 23 is carried only 2 steps: up twice.
    Data from square 1024 must be carried 31 steps.

  How many steps are required to carry the data from the square
  identified in your puzzle input all the way to the access port?

  Your puzzle input is 325489.

*/

/*
  19  18  17  16  15  14  13  12  11  10   9
  20  15  14  13  12  11  10   9   8   7   8
  21  16  11  10   9   8   7   6   5   6   7
  22  17  12   7   6   5   4   3   4   5   6
  23  18  13   8   3   2   1   2   3   4   5
  24  19  14   9   4   0   0   1   2   3   4
  25  20  15  10   5   6   7   0   1   2   3
  26  21  16  11  12  13  14  15   0   1   2
  27  22  17  18  19  20  21  22  23   0   1
  28  23  24  25  26  27  28  29  30  31   0
  29  30  31  32  33  34  35  36  37  38  39 ...

  начальные для каждого кольца ячейки:
  0 1  2  3  4  5 ... ring
  1 2 10 26 50 82 ... 1st element
  0 1  9 25 49 81 ...

  начальный элемент кольца: n^2, n={0, 1, 3, 5, 7, 9, ... , 2k-1}
  количество элементов в кольце k: k>0 ? 8*k : 1
  угловые элементы в кольце k: 2k(n+1)-1, n={0,1,2,3}

  угловые элементы:
  0 1 2   3  4  5 : 周第号
  0 1 3   5  7  9 : 南四
  0 3 7  11 15 19 : 南东
  0 5 11 17 23 29 : 北东
  0 7 15 23 31 39 : 北四

  n = {0, 1, 2, 3}
  k ring number k>0
  1: 2n+1 == 2kn+1
  2: 4n+3 == 2kn+3 ==> 2kn+2k-1 == 2k(n+1)-1
  3: 6n+5 == 2kn+5
  4: k=4: 8(n+1)-1 = 8n+7 = 7, 15, 23, 31

 */

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Scanner; // Scanner (System.in);
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;


public class aoc2017_03 implements AoC2017 {

    public void eval (final String[] args) {
        System.out.println (args[0]);
        System.out.println (first (args[1]));
        System.out.println (second (args[1]));
    }

    
    private int first (final String str) {
        int 魔号 = (int) Integer.parseInt (str); // mo
        if (魔号 < 2) return 0;
        int 步 = get_ring_by_index (魔号); // bu4
        int 步周 = 魔号 - get_ring_pivot_nearest (魔号);
        if (步周 < 0) 步周 = -步周;
        return 步 + 步周;
    }


    private int second (final String str) {
        int 魔号 = (int) Integer.parseInt (str);
        return 魔号;
    }


    private int eval_horisontal_offset (final int ring, final int tail) {
        int 动 = 2 * ring; // dong4
        for (; tail >= 动; tail -= 动);
        return 0;
    }


    private int eval_start_index (final int ring) {
        return eval_start_index (ring - 1);
    }

    private int eval_start_index0 (final int ring) {
        // starting from zero ("0"), not "1".
        if (ring <= 0) return 0;
        if (ring == 1) return 1;
        int 甲 = 2 * ring - 1; // 0 1 3 5 7 9 ... 2r-1
        return 甲 * 甲;
    }


    private int eval_ring_length (final int ring) {
        /** k>0 ? 8k : 1
         */
        return (ring <= 1) ? 1 : (1 << (ring + 1));
    }

    private int eval_ring_length0 (final int ring) {
        return eval_ring_length (ring + 1);
    }


    private int get_ring_by_index (final int idx) {
        if (idx <= 1) return 0;
        int 步;
        for (步 = 2; 步 < idx ; ++步) {
            int 乙 = eval_start_index (步);
            if (idx < 乙) break;
        }
        return 步 - 1;
    }


    private int get_ring_pivot_nearest (final int idx) {
        int 周号 = get_ring_by_index (idx);
        int[] 节点 = get_ring_pivot_nodes (周号);
        int 号 = idx - eval_ring_length (周号 - 1); // положение на кольце
        int 步 = -1;
        for (;;) {
            todo();
        }
        return 步;
    }


    private int[] get_ring_pivot_nodes (final int ring) {
        /** именно, узлы к которым нужно двигаться по-кольцу для
         * "горизонтальной" части числа шагов
         */
        int[] 节点 = new int[4]; // jie2
        for (int 甲 = 0; 甲 < 4; ++甲)
            节点[甲] = ring * 2 * (甲 + 1) - 1;
        return 节点;
    }

}
