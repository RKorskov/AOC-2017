// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-03-22 12:55:51 korskov>

/*
  http://adventofcode.com/2017/day/2

  --- Day 2: Corruption Checksum ---

  As you walk through the door, a glowing humanoid shape yells in your
  direction. "You there! Your state appears to be idle. Come help us
  repair the corruption in this spreadsheet - if we take another
  millisecond, we'll have to display an hourglass cursor!"

  The spreadsheet consists of rows of apparently-random numbers. To
  make sure the recovery process is on the right track, they need you
  to calculate the spreadsheet's checksum. For each row, determine the
  difference between the largest value and the smallest value; the
  checksum is the sum of all of these differences.

  For example, given the following spreadsheet:

  5 1 9 5
  7 5 3
  2 4 6 8

  The first row's largest and smallest values are 9 and 1, and their
  difference is 8.

  The second row's largest and smallest values are 7 and 3, and their
  difference is 4.

  The third row's difference is 6.

  In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.

  What is the checksum for the spreadsheet in your puzzle input?

  --- Part Two ---

  "Great work; looks like we're on the right track after all. Here's a
  star for your effort." However, the program seems a little
  worried. Can programs be worried?

  "Based on what we're seeing, it looks like all the User wanted is
  some information about the evenly divisible values in the
  spreadsheet. Unfortunately, none of us are equipped for that kind of
  calculation - most of us specialize in bitwise operations."

  It sounds like the goal is to find the only two numbers in each row
  where one evenly divides the other - that is, where the result of
  the division operation is a whole number. They would like you to
  find those numbers on each line, divide them, and add up each line's
  result.

  For example, given the following spreadsheet:

  5 9 2 8
  9 4 7 3
  3 8 6 5

    In the first row, the only two numbers that evenly divide are 8
    and 2; the result of this division is 4.

    In the second row, the two numbers are 9 and 3; the result is 3.

    In the third row, the result is 2.

  In this example, the sum of the results would be 4 + 3 + 2 = 9.

  What is the sum of each row's result in your puzzle input?

116	1259	1045	679	1334	157	277	1217	218	641	1089	136	247	1195	239	834
269	1751	732	3016	260	6440	5773	4677	306	230	6928	7182	231	2942	2738	3617
644	128	89	361	530	97	35	604	535	297	599	121	567	106	114	480
105	408	120	363	430	102	137	283	123	258	19	101	181	477	463	279
873	116	840	105	285	238	540	22	117	125	699	953	920	106	113	259
3695	161	186	2188	3611	2802	157	2154	3394	145	2725	1327	3741	2493	3607	4041
140	1401	110	119	112	1586	125	937	1469	1015	879	1798	122	1151	100	926
2401	191	219	607	267	2362	932	2283	889	2567	2171	2409	1078	2247	2441	245
928	1142	957	1155	922	1039	452	285	467	305	506	221	281	59	667	232
3882	1698	170	5796	2557	173	1228	4630	174	3508	5629	4395	180	5100	2814	2247
396	311	223	227	340	313	355	469	229	162	107	76	363	132	453	161
627	1331	1143	1572	966	388	198	2068	201	239	176	1805	1506	1890	1980	1887
3390	5336	1730	4072	5342	216	3823	85	5408	5774	247	5308	232	256	5214	787
176	1694	1787	1586	3798	4243	157	4224	3603	2121	3733	851	2493	4136	148	153
2432	4030	3397	4032	3952	2727	157	3284	3450	3229	4169	3471	4255	155	127	186
919	615	335	816	138	97	881	790	855	89	451	789	423	108	95	116

*/

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
import java.util.Scanner;
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;


public class aoc2017_02 implements AoC2017 {

    public void eval (final String[] args) {
        int[][] 表 = read_input();
        System.out.println (args[0]);
        System.out.println (first (表));
        System.out.println (second (表));
    }

    
    private int first (final int[][] table) {
        int 号总 = 0;
        for (int 甲 = 0; 甲 < table.length; ++甲)
            号总 += get_control_sum (table[甲]);
        return 号总;
    }


    private int second (final int[][] table) {
        int 号总 = 0;
        for (int 甲 = 0; 甲 < table.length; ++甲) {
            int 丁 = 0;
            for (int 乙 = 0; 丁 < 1 || 乙 < table[甲].length; ++乙) {
                for (int 丙 = 乙 + 1; 丙 < table[甲].length; ++丙) {
                    if (table[甲][乙] % table[甲][丙] == 0) {
                        丁 = table[甲][乙] / table[甲][丙];
                        break;
                    }
                    if (table[甲][丙] % table[甲][乙] == 0) {
                        丁 = table[甲][丙] / table[甲][乙];
                        break;
                    }
                }
            }
            if (丁 > 0)
                号总 += 丁;
            else
                System.out.println ("Wat?!");
        }
        return 号总;
    }


    private int[][] read_input () {
        ArrayList <int[]> 表 = new ArrayList <int[]> ();
        Scanner 输入 = new Scanner (System.in); // shu ru
        for (; 输入.hasNextLine(); ) {
            String 串 = 输入.nextLine(); // chuan
            String[] 串串 = 串.split ("\\s+");
            int[] 线 = new int [串串.length]; // xian
            for (int 甲 = 0; 甲 < 串串.length; ++甲)
                线[甲] = Integer.parseInt (串串[甲]);
            表.add (线);
        }
        return 表.toArray (new int[表.size()][]);
    }


    private int get_control_sum (final int[] row) {
        int 号上 = 0, 号下 = 0x7fffffff; // shang xia
        for (int 乙 : row) {
            if (号上 < 乙) 号上 = 乙;
            if (号下 > 乙) 号下 = 乙;
        }
        return 号上 - 号下;
    }

}
